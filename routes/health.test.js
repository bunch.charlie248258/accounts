const request = require('supertest');
const health = require('./health');

function createApp() {
  const app = require('express')();
  app.use('/api/health', health());
  return app;
}

describe('GET /api/health', () => {
  test('when server is running then return status available', async (done) => {
    request(createApp())
      .get('/api/health')
      .expect(200)
      .then(response => {
        expect(response.body.status).toBe('available');
        expect(response.body.uptime).toEqual(expect.any(Number));
        done();
      });
  });
});
