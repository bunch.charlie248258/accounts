const prodConfig = require('./dev');

const config = Object.assign({}, prodConfig)

module.exports = config;
