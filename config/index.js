const prodConfig = require('./dev');
const testConfig = require('./testing');
const devConfig = require('./prod');

const logger = require('../helpers/logger');

switch (process.env.NODE_ENV) {
  case 'production':
    logger.info('Loading production configuration');
    module.exports = prodConfig;
    break;
  case 'test':
    logger.info('Loading testing configuration');
    module.exports = testConfig;
    break;
  default:
    logger.info('Loading development configuration');
    module.exports = devConfig;
    break;
}
